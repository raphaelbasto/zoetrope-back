const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Media = sequelize.define('Media', {
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },

    sinopsis: {
        type: DataTypes.STRING,
        allowNull: false
    },

    score: {
        type: DataTypes.FLOAT
    },

    release_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    genre: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    // timestamps: false
});

Media.associate = function(models) {
    Media.hasMany(models.Review);
    // Media.belongsToMany(models.User, {through: 'watched', as: 'seen_by_list', foreignKey: 'userId'})
}

module.exports = Media;