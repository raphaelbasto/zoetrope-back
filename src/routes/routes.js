const { Router } = require('express');
const UserController = require('../controllers/UserController');
const ReviewController = require('../controllers/ReviewController');
const MediaController = require('../controllers/MediaController');
const router = Router();

router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.get('/seenList/:id',UserController.listSeen);
router.post('/users',UserController.create);
router.put('/users/:id', UserController.update);
router.put('/addSeenList/:id', UserController.watch);
router.put('/removeSeenList/:id', UserController.unwatch);
router.delete('/users/:id', UserController.destroy);

router.get('/reviews',ReviewController.index);
router.get('/reviews/:id',ReviewController.show);
router.get('/reviewsByUserID/:id', ReviewController.allByID);
router.post('/reviews',ReviewController.create);
router.put('/reviews/:id', ReviewController.update);
router.delete('/reviews/:id', ReviewController.destroy)

router.get('/medias',MediaController.index);
router.get('/medias/:id',MediaController.show);
router.get('/mediasByScore/:score',MediaController.findByScore);
router.post('/medias',MediaController.create);
router.put('/medias/:id', MediaController.update);
router.delete('/medias/:id', MediaController.destroy)


module.exports = router;
