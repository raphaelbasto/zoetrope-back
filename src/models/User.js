const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    phone: {
        type: DataTypes.STRING
    },

    favorite_genre: {
        type: DataTypes.STRING
    }

}, {
    // timestamps: false
});

User.associate = function(models) {
    User.hasMany(models.Review);
    User.belongsToMany(models.Media, {through: 'watched', as: 'seen_list', foreignKey: 'userId'})
}

module.exports = User;