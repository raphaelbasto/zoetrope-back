const {response} = require ('express');
const Review = require('../models/Review');
const User = require('../models/User');
const Media = require('../models/Media');


// Review é uma entidade fraca, assim, para ela existir precisa estar ligada a um usuário e uma mídia. Dessa forma, sua criação se torna mais complexa. Além disso, a criação de um review implica na atualização do score de uma mídia.
const create = async(req,res) => {
    try{
          const user = await User.findByPk(req.body.UserId);
          const media = await Media.findByPk(req.body.MediaId);
          const atualScore = media.score;   // encontro a pontuação atual da mídia.
          const count = await Review.count({
              where: {MediumId: req.body.MediaId}
          })    // conto quantos reviews foram feitos sobre a mesma mídia.

          // Contas para a atualização do score.
          const atual = parseFloat(atualScore*count);
          const pontoNovo = parseFloat(req.body.score);
          const countNovo = parseFloat(count+1);
          const newScore = ((atual)+pontoNovo)/(countNovo); // nova média.
          const [updated] = await Media.update({score: newScore}, {where: {id: req.body.MediaId}});
          if(!updated) {    // Se não foi possível atualizar o valor do score, retorna um erro.
            throw new Error();
          } 
          // Se passou por todos os passos acima, está pronto para criar a instância de review.
          const review = await Review.create(req.body);
          await review.setUser(user);   // associa ao usuário
          await review.setMedium(media);    // associa à mídia
          await user.addSeen_list(media);   // guarda o filme na lista de filmes assistidos pelo o usuário
          return res.status(201).json({message: "Avaliação cadastrada com sucesso!", review: review});
      }catch(err){
          res.status(500).json({error: err, message: "Erro ao criar nova Avaliação."});
      }
};

const index = async(req,res) => {
    try {
        const reviews = await Review.findAll();
        return res.status(200).json({message: "Avaliações encontradas.", reviews});
    }catch(err){
        return res.status(500).json({err, message: "Avaliações não encontradas."});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const review = await Review.findByPk(id);
        return res.status(200).json({message: "Avaliação encontrada.", review});
    }catch(err){
        return res.status(500).json({err, message: "Avaliação não encontrada."});
    }
};

const allByID = async(req,res) => {
    const {id} = req.params;
    try {
        const reviews = await Review.findAll({where: {UserId: id}});
        return res.status(200).json({message: "Avaliações encontradas.", reviews});
    }catch(err){
        return res.status(500).json({err, message: "Avaliações não encontradas."});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Review.update(req.body, {where: {id: id}});
        if(updated) {
            const review = await Review.findByPk(id);
            return res.status(200).send(review);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Erro ao tentar atualizar informações de uma Avaliação.");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Review.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Avaliação deletada com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Avaliação não encontrada.");
    }
};

module.exports = {
    index,
    show,
    allByID,
    create,
    update,
    destroy
};
