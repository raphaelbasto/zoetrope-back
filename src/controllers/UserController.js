const {response} = require ('express');
const Media = require('../models/Media');
const User = require('../models/User');

const create = async(req,res) => {
    try{
          const user = await User.create(req.body);
          return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
      }catch(err){
          res.status(500).json({message: "Erro ao cadastrar usuário", error: err});
      }
};

const index = async(req,res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({message: "Usuários encontrados.", users});
    }catch(err){
        return res.status(500).json({message: "Usuários não encontrados", err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({message: "Usuário encontrado.", user});
    }catch(err){
        return res.status(500).json({message: "Usuário não encontrado", err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json(err + "!");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

const watch = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const media = await Media.findByPk(req.body.MediaId);
        if(media) {
            await user.addSeen_list(media);
            return res.status(200).json(user);
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json(err + "!");
    }
};

const unwatch = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const media = await User.findByPk(req.body.MediaId);
        if(media) {
            await user.removeSeen_list(media);
            return res.status(200).json(user);
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json({err});
    }
};

const listSeen = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const watched = await user.getSeen_list();
        return res.status(200).json({watched});
    }catch(err){
        return res.status(500),json(err + "!");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    watch,
    unwatch,
    listSeen
};
