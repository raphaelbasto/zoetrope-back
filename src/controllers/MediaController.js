const {response} = require ('express');
const Media = require('../models/Media');

const create = async(req,res) => {
    try{
          const media = await Media.create(req.body);
          return res.status(201).json({message: "Mídia cadastrada com sucesso!", Media: media});
      }catch(err){
          res.status(500).json({error: err, message: "Erro ao criar nova Mídia."});
      }
};

const index = async(req,res) => {
    try {
        const media = await Media.findAll();
        return res.status(200).json({message: "Mídias encontradas.", media});
    }catch(err){
        return res.status(500).json({err, message: "Mídias não encontradas."});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const media = await Media.findByPk(id);
        return res.status(200).json({message: "Mídia encontrada.", media});
    }catch(err){
        return res.status(500).json({err, message: "Mídia não encontrada."});
    }
};

const findByScore = async(req,res) => {
    const {score} = req.params;
    try {
        const media = await Media.findAll({where: {score: score}});
        return res.status(200).json({message: "Mídias encontradas.", media});
    }catch(err){
        return res.status(500).json({err, message: "Mídias não encontradas."});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Media.update(req.body, {where: {id: id}});
        if(updated) {
            const media = await Media.findByPk(id);
            return res.status(200).send(media);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Erro ao tentar atualizar informações de uma Mídia.");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Media.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Mídia deletada com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Mídia não encontrada.");
    }
};

module.exports = {
    index,
    show,
    findByScore,
    create,
    update,
    destroy
};
