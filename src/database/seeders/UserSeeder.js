const User = require("../../models/User");
const faker = require('faker-br');

 const seedUser = async function () {
   try {
     await User.sync({ force: true });
     const users = [];

     for (let i = 0; i < 10; i++) {

      let user = await User.create({
        username: faker.internet.userName(),
        email: faker.internet.email(),
        name: faker.name.firstName(),
        createdAt: new Date(),
        updatedAt: new Date()
      });

    }

  } catch (err) { console.log(err +'!'); }
}

module.exports = seedUser;