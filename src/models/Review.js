const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Review = sequelize.define('Review', {
    score: {
        type: DataTypes.FLOAT,
        allowNull: false
    },
    
    title: {
        type: DataTypes.STRING
    },

    content: {
        type: DataTypes.STRING
    }

}, {
    // timestamps: false
});

Review.associate = function(models) {
    Review.belongsTo(models.User);
    Review.belongsTo(models.Media);
}

module.exports = Review;